﻿using System;
using BrawlLib.OpenGL;
using System.ComponentModel;
using BrawlLib.SSBB.ResourceNodes;
using System.IO;
using BrawlLib.Modeling;
using System.Drawing;
using BrawlLib.Wii.Animations;
using System.Collections.Generic;
using BrawlLib.SSBBTypes;
using BrawlLib.IO;
using BrawlLib;
using System.Drawing.Imaging;
using Gif.Components;
using OpenTK.Graphics.OpenGL;
using BrawlLib.Imaging;
using System.Text;

namespace System.Windows.Forms
{
    public partial class ModelEditControl : UserControl
    {
        //Updates specified angle by applying an offset.
        //Allows pnlAnim to handle the changes so keyframes are updated.
        private unsafe void ApplyAngle(int index, float offset)
        {
            NumericInputBox box = chr0Editor._transBoxes[index + 3];
            box.Value = (float)Math.Round(box._value + offset, 3);
            chr0Editor.BoxChanged(box, null);
        }
        //Updates translation with offset.
        private unsafe void ApplyTranslation(int index, float offset)
        {
            NumericInputBox box = chr0Editor._transBoxes[index + 6];
            box.Value = (float)Math.Round(box._value + offset, 3);
            chr0Editor.BoxChanged(box, null);
        }
        //Updates scale with offset.
        private unsafe void ApplyScale(int index, float offset)
        {
            NumericInputBox box = chr0Editor._transBoxes[index];
            float value = (float)Math.Round(box._value * offset, 3);
            if (value == 0) return;
            box.Value = value;
            chr0Editor.BoxChanged(box, null);
        }
        private unsafe void ApplyScale2(int index, float offset)
        {
            NumericInputBox box = chr0Editor._transBoxes[index];

            if (box._value == 0)
                return;

            float scale = (box._value + offset) / box._value;

            float value = (float)Math.Round(box._value * scale, 3);
            if (value == 0) return;
            box.Value = value;
            chr0Editor.BoxChanged(box, null);
        }
        public AnimType TargetAnimType
        {
            get { return (AnimType)pnlAssets.fileType.SelectedIndex; }
            set { pnlAssets.fileType.SelectedIndex = (int)value; }
        }
        private Control _currentControl = null;
        public int prevHeight = 0, prevWidth = 0;
        public void ToggleWeightEditor()
        {
            if (weightEditor.Visible)
            {
                panel3.Height = prevHeight;
                panel3.Width = prevWidth;
                weightEditor.Visible = false;
                _currentControl.Visible = true;
            }
            else
            {
                if (vertexEditor.Visible)
                    ToggleVertexEditor();

                prevHeight = panel3.Height;
                prevWidth = panel3.Width;
                panel3.Width = 320;
                panel3.Height = 78;
                weightEditor.Visible = true;
                _currentControl.Visible = false;
            }

            CheckDimensions();
        }
        public void ToggleVertexEditor()
        {
            if (vertexEditor.Visible)
            {
                panel3.Height = prevHeight;
                panel3.Width = prevWidth;
                vertexEditor.Visible = false;
                _currentControl.Visible = true;
            }
            else
            {
                if (weightEditor.Visible)
                    ToggleWeightEditor();

                prevHeight = panel3.Height;
                prevWidth = panel3.Width;
                panel3.Width = 118;
                panel3.Height = 85;
                vertexEditor.Visible = true;
                _currentControl.Visible = false;
            }

            CheckDimensions();
        }
        public void SetCurrentControl()
        {
            Control newControl = null;
            switch (TargetAnimType)
            {
                case AnimType.CHR:
                    newControl = chr0Editor;
                    break;
                case AnimType.SRT:
                    newControl = srt0Editor;
                    syncTexObjToolStripMenuItem.Checked = true;
                    break;
                case AnimType.SHP:
                    newControl = shp0Editor;
                    break;
                case AnimType.PAT:
                    newControl = pat0Editor;
                    syncTexObjToolStripMenuItem.Checked = true;
                    break;
                case AnimType.VIS:
                    newControl = vis0Editor;
                    break;
                case AnimType.SCN:
                    newControl = scn0Editor;
                    break;
                case AnimType.CLR:
                    newControl = clr0Editor;
                    break;
            }
            if (_currentControl != newControl)
            {
                if (_currentControl != null)
                    _currentControl.Visible = false;
                _currentControl = newControl;
                if (!(_currentControl is SRT0Editor) && !(_currentControl is PAT0Editor))
                    syncTexObjToolStripMenuItem.Checked = false;
                if (_currentControl != null)
                {
                    _currentControl.Visible = true;
                    if (_currentControl is CHR0Editor)
                    {
                        animEditors.Height = 78;
                        panel3.Width = 582;
                        pnlKeyframes.SetEditType(0);
                    }
                    else if (_currentControl is SRT0Editor)
                    {
                        animEditors.Height = 78;
                        panel3.Width = 483;
                        pnlKeyframes.SetEditType(0);
                    }
                    else if (_currentControl is SHP0Editor)
                    {
                        animEditors.Height = 106;
                        panel3.Width = 533;
                        pnlKeyframes.SetEditType(0);
                    }
                    else if (_currentControl is PAT0Editor)
                    {
                        animEditors.Height = 78;
                        panel3.Width = 402;
                    }
                    else if (_currentControl is VIS0Editor)
                    {
                        animEditors.Height = 62;
                        panel3.Width = 210;
                        pnlKeyframes.SetEditType(1);
                    }
                    else if (_currentControl is CLR0Editor)
                    {
                        animEditors.Height = 62;
                        panel3.Width = 168;
                        pnlKeyframes.SetEditType(2);
                    }
                    else if (_currentControl is SCN0Editor)
                    {
                        scn0Editor.GetDimensions();
                        pnlKeyframes.SetEditType(3);
                    }
                    else
                        animEditors.Height = panel3.Width = 0;
                }
                else animEditors.Height = panel3.Width = 0;
                return;
            }
            CheckDimensions();
            UpdatePropDisplay();
        }
        public void UpdatePropDisplay()
        {
            if (animEditors.Height == 0 || animEditors.Visible == false)
                return;

            if (TargetAnimType == AnimType.CHR)
            {
                chr0Editor.UpdatePropDisplay();
                chr0Editor.btnInsert.Enabled =
                chr0Editor.btnDelete.Enabled =
                chr0Editor.btnClearAll.Enabled = SelectedCHR0 == null ? false : true;
            }

            if (TargetAnimType == AnimType.SRT)
            {
                srt0Editor.UpdatePropDisplay();
                srt0Editor.btnInsert.Enabled =
                srt0Editor.btnDelete.Enabled =
                srt0Editor.btnClear.Enabled = SelectedSRT0 == null ? false : true;
            }

            if (TargetAnimType == AnimType.VIS)
            {
                if (pnlKeyframes.visEditor.TargetNode != null && !((VIS0EntryNode)pnlKeyframes.visEditor.TargetNode).Flags.HasFlag(VIS0Flags.Constant))
                {
                    pnlKeyframes.visEditor._updating = true;
                    pnlKeyframes.visEditor.listBox1.SelectedIndices.Clear();
                    pnlKeyframes.visEditor.listBox1.SelectedIndex = CurrentFrame;
                    pnlKeyframes.visEditor._updating = false;
                }
            }

            if (TargetAnimType == AnimType.SHP)
                shp0Editor.UpdatePropDisplay();

            if (TargetAnimType == AnimType.PAT)
                pat0Editor.UpdatePropDisplay();

            if (TargetAnimType == AnimType.SCN)
                scn0Editor.UpdatePropDisplay();
        }

        public bool _editingAll { get { return (!(models.SelectedItem is MDL0Node) && models.SelectedItem != null && models.SelectedItem.ToString() == "All"); } }
        public void UpdateModel()
        {
            if (_updating || models == null)
                return;

            if (!_editingAll)
            {
                if (TargetModel != null)
                    UpdateModel(TargetModel);
            }
            else
                foreach (MDL0Node n in _targetModels)
                    UpdateModel(n);

            if (!_playing)
                UpdatePropDisplay();

            modelPanel.Invalidate();
        }
        private void UpdateModel(MDL0Node model)
        {
            if (_chr0 != null && !(TargetAnimType != AnimType.CHR && !playCHR0ToolStripMenuItem.Checked))
                model.ApplyCHR(_chr0, _animFrame);
            else
                model.ApplyCHR(null, 0);
            if (_srt0 != null && !(TargetAnimType != AnimType.SRT && !playSRT0ToolStripMenuItem.Checked))
                model.ApplySRT(_srt0, _animFrame);
            else
                model.ApplySRT(null, 0);
            if (_shp0 != null && !(TargetAnimType != AnimType.SHP && !playSHP0ToolStripMenuItem.Checked))
                model.ApplySHP(_shp0, _animFrame);
            else
                model.ApplySHP(null, 0);
            if (_pat0 != null && !(TargetAnimType != AnimType.PAT && !playPAT0ToolStripMenuItem.Checked))
                model.ApplyPAT(_pat0, _animFrame);
            else
                model.ApplyPAT(null, 0);
            if (_vis0 != null && !(TargetAnimType != AnimType.VIS && !playVIS0ToolStripMenuItem.Checked))
                if (model == TargetModel)
                    ReadVIS0();
                else
                    model.ApplyVIS(_vis0, _animFrame);
            if (_scn0 != null/* && !(TargetFileType != FileType.SCN && !playSCN0ToolStripMenuItem.Checked)*/)
                model.SetSCN0Frame(_animFrame);
            else
                model.SetSCN0Frame(0);
            if (_clr0 != null && !(TargetAnimType != AnimType.CLR && !playCLR0ToolStripMenuItem.Checked))
                model.ApplyCLR(_clr0, _animFrame);
            else
                model.ApplyCLR(null, 0);
        }

        public void AnimChanged(AnimType type)
        {
            //Update animation editors
            if (type != AnimType.SRT) pnlAssets.UpdateSRT0Selection(null);
            if (type != AnimType.PAT) pnlAssets.UpdatePAT0Selection(null);
            if (type != AnimType.SCN)
                foreach (MDL0Node m in _targetModels)
                    m.SetSCN0(null);

            switch (type)
            {
                case AnimType.CHR:
                    break;
                case AnimType.SRT:
                    pnlAssets.UpdateSRT0Selection(SelectedSRT0);
                    break;
                case AnimType.SHP:
                    shp0Editor.UpdateSHP0Indices();
                    break;
                case AnimType.PAT:
                    pat0Editor.UpdateBoxes();
                    pnlAssets.UpdatePAT0Selection(SelectedPAT0);
                    break;
                case AnimType.VIS:
                    vis0Editor.UpdateAnimation();
                    break;
                case AnimType.SCN:
                    //foreach (MDL0Node m in _targetModels)
                    //    m.SetSCN0(_scn0);
                    scn0Editor.tabControl1_Selected(null, new TabControlEventArgs(null, scn0Editor.tabIndex, TabControlAction.Selected));
                    break;
                case AnimType.CLR:
                    clr0Editor.UpdateAnimation();
                    break;
            }

            //Update keyframe panel
            pnlKeyframes.TargetSequence = null;
            btnAnimToggle.Enabled = true;
            switch (TargetAnimType)
            {
                case AnimType.CHR:
                    if (_chr0 != null && SelectedBone != null)
                        pnlKeyframes.TargetSequence = _chr0.FindChild(SelectedBone.Name, false);
                    break;
                case AnimType.SRT:
                    if (_srt0 != null && TargetTexRef != null)
                        pnlKeyframes.TargetSequence = srt0Editor.TexEntry;
                    break;
                case AnimType.SHP:
                    if (_shp0 != null)
                        pnlKeyframes.TargetSequence = shp0Editor.VertexSetDest;
                    break;
                case AnimType.CLR:
                case AnimType.VIS:
                    //if (TargetVisEntry == null) break;
                    //string name = TargetVisEntry.Name;
                    //if (_vis0 != null)
                    //{
                    //    int i = 0;
                    //    foreach (object s in vis0Editor.listBox1.Items)
                    //        if (s.ToString() == name)
                    //            vis0Editor.listBox1.SelectedIndex = i;
                    //        else 
                    //            i++;
                    //}
                    break;
                default:
                    if (pnlKeyframes.Visible)
                        btnAnimToggle_Click(null, null);
                    btnAnimToggle.Enabled = false;
                    break;
            }

            if (GetSelectedBRRESFile(type) == null)
            {
                pnlPlayback.numFrameIndex.Maximum = _maxFrame = 0;
                pnlPlayback.numTotalFrames.Minimum = 0;
                _updating = true;
                pnlPlayback.numTotalFrames.Value = 0;
                _updating = false;
                pnlPlayback.btnPlay.Enabled =
                pnlPlayback.numTotalFrames.Enabled =
                pnlPlayback.numFrameIndex.Enabled = false;
                pnlPlayback.btnLast.Enabled = false;
                pnlPlayback.btnFirst.Enabled = false;
                pnlPlayback.Enabled = false;
                SetFrame(0);
            }
            else
            {
                int oldMax = _maxFrame;

                _maxFrame = ((BRESEntryNode)GetSelectedBRRESFile(type)).tFrameCount;

                _updating = true;
                pnlPlayback.btnPlay.Enabled =
                pnlPlayback.numFrameIndex.Enabled =
                pnlPlayback.numTotalFrames.Enabled = true;
                pnlPlayback.Enabled = true;
                pnlPlayback.numTotalFrames.Value = _maxFrame;
                if (syncLoopToAnimationToolStripMenuItem.Checked)
                    pnlPlayback.chkLoop.Checked = ((BRESEntryNode)GetSelectedBRRESFile(type)).tLoop;
                _updating = false;

                if (_maxFrame < oldMax)
                {
                    SetFrame(1);
                    pnlPlayback.numFrameIndex.Maximum = _maxFrame;
                }
                else
                {
                    pnlPlayback.numFrameIndex.Maximum = _maxFrame;
                    SetFrame(1);
                }
            }
        }

        public bool _playing = false;
        public void SetFrame(int index)
        {
            //if (_animFrame == index)
            //    return;

            if (index > _maxFrame + 1 || index < 0)
                return;

            index = TargetModel == null ? 0 : index;

            CurrentFrame = index;

            if (firstPersonSCN0CamToolStripMenuItem.Checked && _scn0 != null && scn0Editor._camera != null)
            {
                SCN0CameraNode c = scn0Editor._camera;
                CameraAnimationFrame f = c.GetAnimFrame(index - 1);
                Vector3 r = c.GetRotate(index);
                Vector3 t = f.Pos;
                modelPanel._camera.Reset();
                modelPanel._camera.Translate(t._x, t._y, t._z);
                modelPanel._camera.Rotate(r._x, r._y, r._z);
                modelPanel._aspect = f.Aspect;
                modelPanel._farZ = f.FarZ;
                modelPanel._nearZ = f.NearZ;
                modelPanel._fovY = f.FovY;
                modelPanel.OnResized();
            }

            pnlPlayback.btnNextFrame.Enabled = _animFrame < _maxFrame;
            pnlPlayback.btnPrevFrame.Enabled = _animFrame > 0;

            pnlPlayback.btnLast.Enabled = _animFrame != _maxFrame;
            pnlPlayback.btnFirst.Enabled = _animFrame > 1;

            if (_animFrame <= pnlPlayback.numFrameIndex.Maximum)
                pnlPlayback.numFrameIndex.Value = _animFrame;
        }

        private bool wasOff = false;
        public bool runningAction = false;
        private int framesCounted = 0;
        private Vector3 cameraStartPosition;
        private Vector3 cameraStartRotation;

        public void PlayAnim()
        {
            if (GetSelectedBRRESFile(TargetAnimType) == null || _maxFrame == 1)
                return;

            pnlMoveset.scriptEditor1.resetFrameSpeedCheckingStuff();

            if (followCharacterDuringAnimation)
            {
                // Select the TransN bone to center the camera on the character
                foreach (MDL0BoneNode bone in pnlBones.lstBones.Items)
                {
                    if (bone.Name.Equals("TransN"))
                    {
                        pnlBones.SelectedBone = bone;
                        break;
                    }
                }
                cameraStartPosition = modelPanel._camera.GetPoint().Round(3);
                cameraStartPosition = new Vector3(cameraStartPosition._z, cameraStartPosition._y, -cameraStartPosition._x); // z_out is -x_in
                cameraStartRotation = modelPanel._camera._rotation;
            }

            _playing = false;
            RenderBones = false;
            toggleBones.Checked = false;
            EnableTransformEdit = false;
            elapsedInGameFrames = 0;

            pnlMoveset.SetFrame(1);
            if (_capture)
            {
                _loop = false;
                animTimer.Interval = 1;
                _replayFirstFrame = true;
            }

            if (_animFrame < _maxFrame)
            {
                framesCounted = 0;
                frameCountForDuration = 0;
                animTimer.Start();
                pnlPlayback.btnPlay.Text = "Stop";
            }
        }

        public void StopAnim()
        {
            animTimer.Stop();

            _playing = false;

            if (followCharacterDuringAnimation)
            {
                modelPanel._camera.Reset();
                modelPanel._camera.Rotate(cameraStartRotation._x, cameraStartRotation._y, cameraStartRotation._z);
                modelPanel._camera.Translate(cameraStartPosition._x, cameraStartPosition._y, cameraStartPosition._z);
                modelPanel.Invalidate();
            }

            pnlPlayback.btnPlay.Text = "Play";
            EnableTransformEdit = true;
            UpdatePropDisplay();

            if (_capture)
            {
                WriteMoveInfoToFile();
                _capture = false;
                if (_isDoingMultiAnimationDump)
                {
                    continueDumpingAnimations();
                }
            }
        }

        bool _replayFirstFrame = false;
        float elapsedInGameFrames = 0;
        int frameCountForDuration = 0;
        private void animTimer_Tick(object sender, EventArgs e)
        {
            framesCounted++;
            if (GetSelectedBRRESFile(TargetAnimType) == null)
                return;

            if (followCharacterDuringAnimation)
            {
                Vector3 transNBonePosition = getTransNBonePosition();
                Vector3 newCameraPosition = cameraStartPosition + transNBonePosition;

                modelPanel._camera.Reset();
                modelPanel._camera.Rotate(cameraStartRotation._x, cameraStartRotation._y, cameraStartRotation._z);
                modelPanel._camera.Translate(newCameraPosition._x, newCameraPosition._y, newCameraPosition._z);
                modelPanel.Invalidate();
            }

            bool repeatFrame = false;
            if (_capture)
            {
                int brawlBoxFrame = _animFrame - 1;
                if (_animFrame > 1)
                {
                    float frameSpeed = pnlMoveset.scriptEditor1.FrameSpeedForCurrentFrame;
                    elapsedInGameFrames += frameSpeed;

                    String comment = "";
                    if (elapsedInGameFrames < brawlBoxFrame && brawlBoxFrame != 1)
                    {
                        if (!skipAnimationsAndExportData)
                        {
                            DuplicateFrame(_animFrame - 1);
                        }
                        repeatFrame = true;
                        comment = " - Duplicate previous frame (if there is one)";
                        frameCountForDuration++;
                    }
                    else if (elapsedInGameFrames > brawlBoxFrame + 1)
                    {
                        comment = " - Skipping this frame";
                        elapsedInGameFrames -= frameSpeed;
                    }
                    else
                    {
                        frameCountForDuration++;
                        if (!skipAnimationsAndExportData)
                        {
                            DumpImageToFolder(modelPanel.GrabScreenshot(false), _animFrame);
                        }
                    }
                    if (!comment.Equals(""))
                    {
                        String actionName = pnlMoveset.SelectedObject.ToString();
                        //Console.WriteLine("{0}: BrawlboxFrame: {1}, time: {2}{3}", actionName, brawlBoxFrame, elapsedInGameFrames, comment);
                    }
                }
            }

            if (_animFrame >= _maxFrame || framesCounted > _maxFrame + 30)
            {
                if (!_loop)
                    StopAnim();
                else
                    pnlMoveset.SetFrame(1);
            }
            else
            {
                if (_replayFirstFrame)
                {
                    _animFrame = 1;
                    pnlMoveset.SetFrame(1);
                    _replayFirstFrame = false;
                    elapsedInGameFrames = 0;
                }
                else
                {
                    int nextFrame = repeatFrame ? _animFrame : _animFrame + 1;
                    pnlMoveset.SetFrame(nextFrame);
                }
            }
        }

        private Vector3 getTransNBonePosition()
        {
            String xStr = chr0Editor._transBoxes[6].Text;
            String yStr = chr0Editor._transBoxes[7].Text;
            String zStr = chr0Editor._transBoxes[8].Text;

            try
            {
                float x = float.Parse(xStr);
                float y = float.Parse(yStr);
                float z = float.Parse(zStr);
                return new Vector3(z, y, x); // Have to reverse this
            }
            catch (Exception parseFail)
            {
                Console.WriteLine("Failed to parse one of ({0}, {1}, {2}) to float. \n\n  Stack Trace:\n{3}", xStr, yStr, zStr, parseFail.StackTrace.ToString());
            }

            return Vector3.Zero;
        }

        private void WriteMoveInfoToFile()
        {
            String moveEventInfo = pnlMoveset.scriptEditor1.GetSelectedMoveEventListInfo();
            String[] events = moveEventInfo.Replace("\r", "").Split('\n');

            float currentFSM = 1.0f;
            float countedFramesBeforeMostRecentFSMChange = 0.0f;
            float frameOfMostRecentFSMChange = 0;
            int inGameStartFrame = 0;
            float brawlBoxStartFrame = 0;

            // Hitboxes
            int collisionStart = -1;
            List<Tuple<int, int>> hitboxes = new List<Tuple<int, int>>();
            HashSet<String> activeHitboxIDs = new HashSet<String>();
            List<String> offensiveCollisionStrings = new List<String>();
            List<List<String>> offensiveCollisions = new List<List<String>>();
            // Grab Boxes
            int grabStart = -1;
            List<Tuple<int, int>> grabBoxes = new List<Tuple<int, int>>();
            List<String> catchCollisionStrings = new List<String>();
            List<List<String>> catchCollisions = new List<List<String>>();

            int boneStatusStart = -1;
            String bonesInStatus = "";
            String boneStatusType = "";
            List<Tuple<String, Tuple<int, int>, String>> boneStatus = new List<Tuple<String, Tuple<int, int>, String>>();
            int armorStartFrame = -1;
            String armorType = "";
            String armorTolerance = "";
            List<Tuple<String, Tuple<int, int>, String>> armorTimes = new List<Tuple<String, Tuple<int, int>, String>>();

            
            
            int intangibleStartFrame = -1;
            int intangibleEndFrame = -1;

            int throwFrame = -1;
            String throwInfo = "";

            int IASAFrame = -1;
            int autoCancelPre = -1;
            int autoCancelPost = -1;

            int numberOfSpeedMods = 0;

            for (int i = 0; i < events.Length; i++)
            {
                Boolean isLastEvent = (i == events.Length - 1);
                String thisEvent = events[i];
                try
                {
                    if (thisEvent.Contains("Frame Speed Modifier:"))
                    { /* Example: Frame Speed Modifier: Multiplier=2.2223x  */
                        String rawValue = thisEvent.Replace("Frame Speed Modifier: Multiplier=", "").Replace("x", "");
                        float frameSinceFSMChange = brawlBoxStartFrame - frameOfMostRecentFSMChange;
                        float framesElapsedInThatWindow = frameSinceFSMChange / currentFSM;
                        countedFramesBeforeMostRecentFSMChange += framesElapsedInThatWindow;

                        frameOfMostRecentFSMChange = brawlBoxStartFrame;
                        currentFSM = float.Parse(rawValue);
                        numberOfSpeedMods++;
                        //Console.WriteLine("The new Frame Speed Modifier is: {0}", currentFrameSpeed);
                    }
                    else if (thisEvent.Contains("Asynchronous Timer:"))
                    { /* Example: Asynchronous Timer: frames=9  */
                        String rawValue = thisEvent.Replace("Asynchronous Timer: frames=", "");
                        float startFrame = float.Parse(rawValue);
                        brawlBoxStartFrame = startFrame;
                        //Console.WriteLine("Async Timer Starting at {0}", (startFrame / currentFrameSpeed) + startFrameAdjustment);
                        float gameStartFrame = countedFramesBeforeMostRecentFSMChange + ((startFrame - frameOfMostRecentFSMChange) / currentFSM);
                        double roundedFrame = Math.Round(gameStartFrame);

                        inGameStartFrame = (int)(roundedFrame); // TODO: make sure this rounds correctly
                    }
                    else if (thisEvent.Contains("Synchronous Timer:"))
                    { /* Example: Synchronous Timer: frames=2  */
                        String rawValue = thisEvent.Replace("Synchronous Timer: frames=", "");
                        float frames = float.Parse(rawValue);
                        brawlBoxStartFrame = frames;
                        int newStartFrame = (int)(inGameStartFrame + (frames / currentFSM));
                        //Console.WriteLine("Sync Timer Starting at {0}", newStartFrame);
                        inGameStartFrame = newStartFrame; // TODO: make sure this rounds correctly
                    }
                    else if (thisEvent.Contains("Offensive Collision:"))
                    { /* Example: Offensive Collision: Id=0, Bone=RKneeJ, Damage=12, ShieldDamage=0, Direction=80, BaseKnockback=30, WeightKnockback=0, KnockbackGrowth=100, Size=3.91, Z Offset=0, Y Offset=8.5, X Offset=0, TripRate=0%, HitlagMultiplier=x1, SDIMultiplier=x1, Flags=964887680  */
                        //Console.WriteLine("Offensive Collision Starting at {0}", inGameStartFrame);
                        int index = thisEvent.IndexOf("=");
                        String id = thisEvent.Substring(index + 1, 1);

                        if (!activeHitboxIDs.Contains(id))
                        {
                            activeHitboxIDs.Add(id);
                        }
                        else
                        {
                            if (offensiveCollisionStrings.Count > 0)
                            {
                                hitboxes.Add(new Tuple<int, int>(collisionStart, inGameStartFrame));
                                offensiveCollisions.Add(offensiveCollisionStrings);
                                offensiveCollisionStrings = new List<String>();
                            }
                            activeHitboxIDs.Clear();
                            collisionStart = -1;
                        }

                        if (collisionStart == -1)
                        {
                            collisionStart = inGameStartFrame;
                        }
                        offensiveCollisionStrings.Add(thisEvent);

                    }
                    else if (thisEvent.Contains("Terminate Collisions") || isLastEvent)
                    { /* Example: Terminate Collisions  */
                        //Console.WriteLine("Terminate Collisions at {0}", inGameStartFrame);
                        if (offensiveCollisionStrings.Count > 0)
                        {
                            hitboxes.Add(new Tuple<int, int>(collisionStart, isLastEvent ? 999 : inGameStartFrame));
                            offensiveCollisions.Add(offensiveCollisionStrings);
                            offensiveCollisionStrings = new List<String>();
                        }
                        activeHitboxIDs.Clear();
                        collisionStart = -1;
                    }
                    if (thisEvent.Contains("Terminate Catch Collisions") || isLastEvent)
                    { /* Terminate Catch Collisions  */
                        //Console.WriteLine("Terminate Catch Collisions at {0}", inGameStartFrame);
                        if (catchCollisionStrings.Count > 0)
                        {
                            grabBoxes.Add(new Tuple<int, int>(grabStart, isLastEvent ? 999 : inGameStartFrame));
                            catchCollisions.Add(catchCollisionStrings);
                            catchCollisionStrings = new List<String>();
                        }
                        grabStart = -1;
                    }
                    else if (thisEvent.Contains("Catch Collision") && !thisEvent.Contains("Delete"))
                    { /*Catch Collision 2: Value-1, Value-0, Scalar-2, Scalar-0, Scalar-8.75, Scalar-7.65, Value-231, Value-3, Value-2,  */
                        //Console.WriteLine("Catch Collision Starting at {0}", inGameStartFrame);
                        if (grabStart == -1)
                        {
                            grabStart = inGameStartFrame;
                        }
                        catchCollisionStrings.Add(thisEvent);

                    }
                    else if (thisEvent.Contains("Undo Bone Collision:"))
                    { /* Example: Undo Bone Collision: Value-0 */
                        //Console.WriteLine("End Bone Collision change at {0}", inGameStartFrame);
                        if (boneStatusStart != -1)
                        {
                            boneStatus.Add(new Tuple<String, Tuple<int, int>, String>(bonesInStatus, new Tuple<int, int>(boneStatusStart, inGameStartFrame), boneStatusType));
                            bonesInStatus = "";
                            boneStatusType = "";
                            boneStatusStart = -1;
                        }
                    }
                    else if (thisEvent.Contains("Bone Collision:"))
                    { /* Example: Bone Collision: bone=LArmJ, status=Intangible */
                        String rawValue = thisEvent.Replace("Bone Collision: ", "");
                        String[] splitValues = rawValue.Split(',');
                        String rawBone = splitValues[0];
                        String rawStatus = splitValues[1];

                        String bone = rawBone.Replace("bone=", "");
                        String status = rawStatus.Replace(" status=", "");

                        if (status.Equals("Normal"))
                        {
                            if (boneStatusStart != -1)
                            {
                                boneStatus.Add(new Tuple<String, Tuple<int, int>, String>(bonesInStatus, new Tuple<int, int>(boneStatusStart, inGameStartFrame), boneStatusType));
                                bonesInStatus = "";
                                boneStatusType = "";
                                boneStatusStart = -1;
                            }
                        }
                        else
                        {
                            if (!status.Equals("Intangible") && !status.Equals("Invincible"))
                            {
                                Console.WriteLine("Unrecognized Bone Status: '{0}'", status);
                            }

                            if (boneStatusStart == -1)
                            {
                                boneStatusStart = inGameStartFrame;
                            }
                            if (!bonesInStatus.Equals(""))
                            {
                                bonesInStatus += "&";
                            }
                            bonesInStatus += bone;
                            bonesInStatus.Trim();
                            boneStatusType = status;
                        }
                    }
                    else if (thisEvent.Equals("Bit Variable Set: RA-Bit[30] = true"))
                    { /* Example: Bit Variable Set: RA-Bit[30] = true  */
                        //Console.WriteLine("Can AutoCancel before: {0}", inGameStartFrame);
                        if (autoCancelPre != -1)
                        {
                            Console.WriteLine("AutoCancel Pre is getting reset?!??!?");
                        }
                        autoCancelPre = inGameStartFrame;
                    }
                    else if (thisEvent.Equals("Bit Variable Clear: RA-Bit[30] = false"))
                    { /* Example: Bit Variable Clear: RA-Bit[30] = false */
                        //Console.WriteLine("Can autoCancel after: {0}", inGameStartFrame);
                        if (autoCancelPost != -1)
                        {
                            Console.WriteLine("AutoCancel Post is getting reset?!??!?");
                        }
                        autoCancelPost = inGameStartFrame;
                    }
                    else if (thisEvent.Contains("Allow Interrupt"))
                    { /* Example: Allow Interrupt  */
                        //Console.WriteLine("IASA: {0}", inGameStartFrame);
                        IASAFrame = inGameStartFrame;
                    }
                    if (isLastEvent && armorStartFrame >= 0)
                    {
                        armorTimes.Add(new Tuple<String, Tuple<int, int>, String>(armorType, new Tuple<int, int>(armorStartFrame, 999), armorTolerance));
                    }
                    if (thisEvent.Contains("Super/Heavy Armor:"))
                    { /* Super/Heavy Armor: State=Super Armor, Tolerance=0
                       * Super/Heavy Armor: State=None, Tolerance=0  */
                        
                        String rawValue = thisEvent.Replace("Super/Heavy Armor: ", "");
                        String[] splitValues = rawValue.Split(',');
                        String rawArmor = splitValues[0];
                        String rawTolerance = splitValues[1];

                        String armor = rawArmor.Replace("State=", "");
                        String tolerance = rawTolerance.Replace(" Tolerance=", "");

                        //Console.WriteLine("Giving Armor: {0} with {1} Tolerance Starting at {2}", armor, tolerance, inGameStartFrame);

                        if (armor.Equals("None"))
                        {
                            if (armorType.Equals(""))
                            {
                                armorType = "UNKNOWN_ARMOR_TYPE";
                            }
                            if (armorTolerance.Equals(""))
                            {
                                armorTolerance = "-1";
                            }
                            armorTimes.Add(new Tuple<String, Tuple<int, int>, String>(armorType, new Tuple<int, int>(armorStartFrame, inGameStartFrame), armorTolerance));
                            armorStartFrame = -1;
                        }
                        else
                        {
                            if (armorStartFrame == -1)
                            {
                                armorStartFrame = inGameStartFrame;
                            }
                            armorType = armor;
                            armorTolerance = tolerance;
                        }

                    }
                    else if (thisEvent.Contains("Throw Specifier:"))
                    { /* "Throw Specifier: ID=0, Bone?=0, Damage=8, Direction=45, KnockbackGrowth=105, WeightKnockback=0,BaseKnockback=45, Element=19, UnknownA=0, UnknownB=1, UnknownC=1, UnknownD=0, SFX=0, Direction?=3, UnknownE=true, UnknownF=true, UnknownG=8" */
                        String id = thisEvent.Substring(thisEvent.IndexOf("ID=") + 3, 1);
                        if (id.Equals("0"))
                        {
                            throwInfo = thisEvent;
                        }
                    }
                    else if (thisEvent.Contains("Throw Applier:"))
                    {
                        throwFrame = inGameStartFrame;
                    }
                    else if (thisEvent.Equals("Body Collision: status=Intangible"))
                    {
                        intangibleStartFrame = inGameStartFrame;
                    }
                    else if (thisEvent.Equals("Body Collision: status=Normal"))
                    {
                        intangibleEndFrame = inGameStartFrame;
                    }
                    else
                    { /* Example:   */
                        //Console.WriteLine("I Don't know how to parse: {0}", thisEvent);
                    }

                }
                catch (Exception e)
                {
                    Console.WriteLine("Failed to parse something about: {0} \n\n {1}", thisEvent, e.StackTrace.ToString());
                }
            }

            // Print it all out
            StringBuilder moveDataString = new StringBuilder();
            moveDataString.AppendFormat("Duration: {0}", frameCountForDuration);
            // Process Hitboxes
            if (hitboxes.Count > 0)
            {
                moveDataString.Append(", Hitboxes: [");
                for (int i = 0; i < hitboxes.Count; i++)
                {
                    Tuple<int, int> hbox = hitboxes[i];
                    moveDataString.AppendFormat("[{0}, {1}]", hbox.Item1 + 1, hbox.Item2);
                    if (i < hitboxes.Count - 1)
                    {
                        moveDataString.Append(", ");
                    }
                }
                moveDataString.Append("]");
            }
            // Process Grab Boxes
            if (grabBoxes.Count > 0)
            {
                moveDataString.Append(", GrabBoxes: [");
                for (int i = 0; i < grabBoxes.Count; i++)
                {
                    Tuple<int, int> gbox = grabBoxes[i];
                    moveDataString.AppendFormat("[{0}, {1}]", gbox.Item1 + 1, gbox.Item2);
                    if (i < grabBoxes.Count - 1)
                    {
                        moveDataString.Append(", ");
                    }
                }
                moveDataString.Append("]");
            }
            if (throwFrame > -1)
            {
                moveDataString.AppendFormat(", ThrowApplied: {0}", throwFrame);
            }
            // Process Bone Status Changes
            if (boneStatus.Count > 0)
            {
                moveDataString.Append(", BoneModifiers: [");
                for (int i = 0; i < boneStatus.Count; i++)
                {
                    Tuple<String, Tuple<int, int>, String> boneEvent = boneStatus[i];

                    moveDataString.AppendFormat("{{ bones: '{0}', status: '{1}', frames: [", boneEvent.Item1, boneEvent.Item3);
                    Tuple<int, int> boneFrames = boneEvent.Item2;

                    moveDataString.AppendFormat("[{0}, {1}]]}}", boneFrames.Item1 + 1, boneFrames.Item2);
                    if (i < boneStatus.Count - 1)
                    {
                        moveDataString.Append(", ");
                    }
                }
                moveDataString.Append("]");
            }
            // Process Armor Changes
            if (armorTimes.Count > 0)
            {
                moveDataString.Append(", Armor: [");
                for (int i = 0; i < armorTimes.Count; i++)
                {
                    Tuple<String, Tuple<int, int>, String> armorEvent = armorTimes[i];
                    Tuple<int, int> armorFrames = armorEvent.Item2;
                    if (i > 0)
                    {
                        moveDataString.Append(", ");
                    }
                    if (armorEvent.Item3.Equals(""))
                    {
                        Console.WriteLine("Fail");
                    }
                    moveDataString.AppendFormat("{{ArmorType: '{0}', Tolerance: {1}, Frames: [{2}, {3}]}}", armorEvent.Item1, armorEvent.Item3, armorFrames.Item1 + 1, armorFrames.Item2);
                }
                moveDataString.Append("]");
            }
            // Process IASA
            if (IASAFrame != -1)
            {
                moveDataString.AppendFormat(", IASA: {0}", IASAFrame + 1);
            }
            if (intangibleStartFrame != -1 && intangibleEndFrame != -1)
            {
                moveDataString.AppendFormat(", Intangibility: [{0}, {1}]", intangibleStartFrame + 1, intangibleEndFrame);
            }
            // Process Auto Cancel Window
            if (autoCancelPre != -1 || autoCancelPost != -1)
            {
                moveDataString.Append(", AutoCancel: [");
                if (autoCancelPre != -1)
                {
                    moveDataString.AppendFormat("[0, {0}]", autoCancelPre + 1);
                }
                if (autoCancelPost != -1)
                {
                    moveDataString.AppendFormat(", [{0}, {1}]", autoCancelPost, frameCountForDuration);
                }
                moveDataString.Append("]");
            }

            if (offensiveCollisions.Count > 0)
            {
                moveDataString.Append(", \n \nDetailedHitboxData: [");

                List<String>[] offensiveCollisionArray = offensiveCollisions.ToArray();
                Tuple<int, int>[] hitboxFramesArray = hitboxes.ToArray();

                for (int i = 0; i < offensiveCollisionArray.Length; i++)
                {
                    moveDataString.AppendFormat("{{ startFrame: {0}, endFrame:{1}, collisions: [", hitboxFramesArray[i].Item1 + 1, hitboxFramesArray[i].Item2);
                    int count = 0;
                    foreach (String collisionText in offensiveCollisionArray[i])
                    {
                        moveDataString.AppendFormat("'{0}'", collisionText);
                        count++;
                        if (count < offensiveCollisionArray[i].Count)
                        {
                            moveDataString.Append(", ");
                        }
                    }
                    moveDataString.Append("] }");
                    if (i < offensiveCollisionArray.Length - 1)
                    {
                        moveDataString.Append(", ");
                    }
                }

                moveDataString.Append("]");
            }

            if (catchCollisions.Count > 0)
            {
                moveDataString.Append(", \n \nDetailedCatchData: [");

                List<String>[] catchCollisionArray = catchCollisions.ToArray();
                Tuple<int, int>[] catchFramesArray = grabBoxes.ToArray();

                for (int i = 0; i < catchCollisionArray.Length; i++)
                {
                    moveDataString.AppendFormat("{{ startFrame: {0}, endFrame:{1}, catchCollisions: [", catchFramesArray[i].Item1 + 1, catchFramesArray[i].Item2);
                    int count = 0;
                    foreach (String collisionText in catchCollisionArray[i])
                    {
                        moveDataString.AppendFormat("'{0}'", collisionText);
                        count++;
                        if (count < catchCollisionArray[i].Count)
                        {
                            moveDataString.Append(", ");
                        }
                    }
                    moveDataString.Append("] }");
                    if (i < catchCollisionArray.Length - 1)
                    {
                        moveDataString.Append(", ");
                    }
                }

                moveDataString.Append("]");
            }

            if (throwFrame > 0)
            {
                moveDataString.AppendFormat(",\n \nDetailedThrowData: '{0}'", throwInfo);
            }



            if (moveDataString.ToString().Length < 13)
            {
                String outPath = GetMoveDataFilePath();
                if (File.Exists(outPath))
                {
                    using (StreamReader reader = new StreamReader(outPath))
                    {
                        if (reader.ReadToEnd().Length > moveDataString.ToString().Length)
                        {
                            Console.WriteLine("Already have data for {0}", GetSelectedActionName());
                            return;
                        }
                    }
                }
            }

            if (moveDataString.ToString().Length > 0)
            {
                DumpMoveDataToFolder(moveDataString.ToString());
            }

            if (numberOfSpeedMods > 2)
            {
                AddSpeedModWarningToFile(numberOfSpeedMods);
            }
        }
    }
}
